

   
function form_inicio(){    

    ajax.url = html.url.absolute()+'/consulta/tabdata/htmf/det.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "det_form" ).innerHTML =  ajax.public.html();       


    form_det_html();
    form_det_data_WS();
    
}
        
        
        
        
    

function form_det_html(){
    
    var f_clase = document.getElementById('f_clase');                
    var f_programa = document.getElementById('f_programa');
    var f_actividad = document.getElementById('f_actividad');
    
    var f_obj = document.getElementById('f_obj');
    var f_ff = document.getElementById('f_ff');
    var f_of = document.getElementById('f_of');
    var f_dpto = document.getElementById('f_dpto');
    
    
    f_clase.addEventListener('keyup', function(event) {
            if(event.keyCode == 13){
                form_det_data_WS();
                f_programa.focus();
                f_programa.select();
            }
        },
        false
    );    
    
    
    f_programa.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_actividad.focus();
                f_actividad.select();
            }
        },
        false
    );    
    
    
    f_actividad.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_obj.focus();
                f_obj.select();
            }
        },
        false
    );    
    
    
    
    f_obj.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_ff.focus();
                f_ff.select();
            }
        },
        false
    );    
    
    
    f_ff.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_of.focus();
                f_of.select();
            }
        },
        false
    );    
    
    
    f_of.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_dpto.focus();
                f_dpto.select();
            }
        },
        false
    );    
    
    
    f_dpto.addEventListener('keyup', function(event) {
            if(event.keyCode == 13) {
                form_det_data_WS();
                f_clase.focus();
                f_clase.select();
            }
        },
        false
    );    
    
    
    
    
    
    
    
    
    
}




   
   
function form_det_data_WS(){

    
        var data = 0;


        var apipath = "/api/presupuesto/consulta/tabdata;n1="+document.getElementById( "f_clase" ).value
                +";n2="+document.getElementById( "f_programa" ).value
                +";n3="+document.getElementById( "f_actividad" ).value
                +";obj="+document.getElementById( "f_obj" ).value                                    
                +";ff="+document.getElementById( "f_ff" ).value
                +";of="+document.getElementById( "f_of" ).value
                +";dpto="+document.getElementById( "f_dpto" ).value
        
        
        
    
    
        loader.inicio();
        xhr =  new XMLHttpRequest();
        
        
        var url = html.url.absolute() + apipath;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                //sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:

                        form_datos_UI ( xhr.responseText);            
                        break;

                    case 204:                            

                        form_datos_UI_limpiar (  );
                        break;
                        
                    case 401:                            
                        window.location = html.url.absolute();                                     
                        break;                        

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );    
      
    
}   
   
   
   
  
  






function form_datos_UI ( json_root ){

        form.name = "form_ordencompra";
        
        //form.json = json;           
        //form.llenar();         
        
        form_datos_UI_totales(json_root);
  
        var ojson = JSON.parse(json_root) ;
        
        var json = JSON.stringify(ojson['data']);
        
//console.log(json);        
        
        
        tabla.json = json;    
                tabla.id = "consulta-tabla";
                tabla.linea = "ff";
                tabla.tbody_id = "consulta-tb";
                tabla.campos = ['clase', 'programa', 'actividad', 'obj', 'ff', 'of', 'dpto',
                    'inicial', 'modifi', 'vigente', 'planfinanciero', 'obligado', 'saldo', 'saldo_plan',
                    'pagado', 'pendiente', 'prevision', 'compromiso'];                
                
                tabla.etiquetas  = ['clase', 'programa', 'actividad', 'obj', 'ff', 'of', 'dpto',
                    'inicial', 'modifi', 'vigente', 'planfinanciero', 'obligado', 'saldo', 'saldo_plan',
                    'pagado', 'pendiente', 'prevision', 'compromiso'];                                
                
                
                //tabla.etiquetas = ['obj', 'vigente', 'prevision', 'compromiso', 'obligado'];
  
        
    //    tabla.ini(obj);
        tabla.gene();   
        
        var obj = new Object();
        obj.tablaformat = ['N', 'N', 'N', 'N', 'N',  'N','N','N','N','N','N','N','N','N','N','N','N','N' ];                       
        tabla.formato(obj);
         
    
            
        
        
};






function form_datos_UI_totales ( json_root ){
    

         //  suma

    
        var ojson = JSON.parse(json_root) ;         
        
            //var table = document.getElementById( "consulta-tabla" ).getElementsByTagName('tbody')[0];

        var sum_inicial = ojson['summary'][0]['sum_inicial']  ;               
        document.getElementById( "sum_inicial" ).innerHTML =  fmtNum(sum_inicial.toString());
                
        var sum_modifi = ojson['summary'][0]['sum_modifi']  ;               
        document.getElementById( "sum_modifi" ).innerHTML =  fmtNum(sum_modifi.toString());
    
        var sum_vigente = ojson['summary'][0]['sum_vigente']  ;               
        document.getElementById( "sum_vigente" ).innerHTML =  fmtNum(sum_vigente.toString());
    
        var sum_planfinanciero = ojson['summary'][0]['sum_planfinanciero']  ;               
        document.getElementById( "sum_planfinanciero" ).innerHTML =  fmtNum(sum_planfinanciero.toString());
    
        var sum_obligado = ojson['summary'][0]['sum_obligado']  ;               
        document.getElementById( "sum_obligado" ).innerHTML =  fmtNum(sum_obligado.toString());
    
        var sum_obligado = ojson['summary'][0]['sum_obligado']  ;               
        document.getElementById( "sum_obligado" ).innerHTML =  fmtNum(sum_obligado.toString());
    
        var sum_saldo = ojson['summary'][0]['sum_saldo']  ;               
        document.getElementById( "sum_saldo" ).innerHTML =  fmtNum(sum_saldo.toString());
    
        var sum_saldo_plan = ojson['summary'][0]['sum_saldo_plan']  ;               
        document.getElementById( "sum_saldo_plan" ).innerHTML =  fmtNum(sum_saldo_plan.toString());
    
        var sum_pagado = ojson['summary'][0]['sum_pagado']  ;               
        document.getElementById( "sum_pagado" ).innerHTML =  fmtNum(sum_pagado.toString());
    
        var sum_pagado = ojson['summary'][0]['sum_pagado']  ;               
        document.getElementById( "sum_pagado" ).innerHTML =  fmtNum(sum_pagado.toString());
    
        var sum_pendiente = ojson['summary'][0]['sum_pendiente']  ;               
        document.getElementById( "sum_pendiente" ).innerHTML =  fmtNum(sum_pendiente.toString());
    
        var sum_prevision = ojson['summary'][0]['sum_prevision']  ;               
        document.getElementById( "sum_prevision" ).innerHTML =  fmtNum(sum_prevision.toString());
    
        var sum_compromiso = ojson['summary'][0]['sum_compromiso']  ;               
        document.getElementById( "sum_compromiso" ).innerHTML =  fmtNum(sum_compromiso.toString());
    
    
}










function form_datos_UI_limpiar (  ){

        form.name = "form_ordencompra";
        
        var json = ("[]");
        
        tabla.json = json;    
                tabla.id = "consulta-tabla";
                tabla.linea = "ff";
                tabla.tbody_id = "consulta-tb";
                tabla.campos = ['clase', 'programa', 'actividad', 'obj', 'ff', 'of', 'dpto',
                    'inicial', 'modifi', 'vigente', 'planfinanciero', 'obligado', 'saldo', 'saldo_plan',
                    'pagado', 'pendiente', 'prevision', 'compromiso'];                
                
                tabla.etiquetas  = ['clase', 'programa', 'actividad', 'obj', 'ff', 'of', 'dpto',
                    'inicial', 'modifi', 'vigente', 'planfinanciero', 'obligado', 'saldo', 'saldo_plan',
                    'pagado', 'pendiente', 'prevision', 'compromiso'];                                
          
        tabla.gene();   


        document.getElementById( "sum_inicial" ).innerHTML =  fmtNum("0");
                
        document.getElementById( "sum_modifi" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_vigente" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_planfinanciero" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_obligado" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_obligado" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_saldo" ).innerHTML = fmtNum("0");
    
        document.getElementById( "sum_saldo_plan" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_pagado" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_pagado" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_pendiente" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_prevision" ).innerHTML =  fmtNum("0");
    
        document.getElementById( "sum_compromiso" ).innerHTML =  fmtNum("0");
    

            
        
        
};








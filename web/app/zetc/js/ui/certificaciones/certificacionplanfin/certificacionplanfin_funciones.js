

   
   
function serie_inicio ( obj, tipo_certi ){        
    
    //busqueda.custom( obj );          

    boton.objeto = obj.nombre;         

    var url = "";
    if (!(obj.alias === undefined)) {    
        url = html.url.absolute() + obj.carpeta +'/'+ obj.alias + '/htmf/serie.html';    
    }              
    else {
        url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/serie.html';    
    }


    
    fetch( url)
      .then(response => {
        return response.text();
      })
      .then(data => {

        document.getElementById( obj.dom ).innerHTML =  data;
        reflex.getTituloplu(obj);

        // combobox
        certificacionTipo_combobox ( "tipo_certificacion", obj, tipo_certi ) ;


      })            

    

    //lista_serie(obj, 1);        
 
    
    
    
}
   






function lista_serie ( obj, tipocertificacion ){    

    ajax.url = html.url.absolute()+'/api/certificacionplanfin/lista/' + tipocertificacion   ;   

    ajax.metodo = "GET";        
    reflex.json = ajax.private.json();    
 
 
 
    var ojson = JSON.parse( reflex.json ) ;  
    reflex.json = JSON.stringify(ojson['datos']) ;  

 
    serie.ini(obj);
    serie.gene();
    
    
    lista_registro_accion(obj, obj.form_id);

    var btn_insertar = document.getElementById('btn_insertar');
    btn_insertar.addEventListener('click',
        function(event) {  
            
            obj.new( obj, tipocertificacion );
            //alert("nuevo");

        },
        false
    );              



}
  
  
  
  
    
  
  
    function lista_registro_accion ( obj ,fn )  {          
            
    
            
        var ulineas =  document.getElementById( "certificacionplanfin_serie_body" ).getElementsByClassName('serie_line');

        for (var i=0 ; i < ulineas.length; i++)
        {
            ulineas[i].onclick = function()
            {                  
                var linea_id = this.dataset.linea_id;                                    
                fn( obj, linea_id );                    
            };       
        }        
        
        
        //document.getElementById("certificacionplanfin_clase").disabled =  false;
        
        
    }
    
  
  
  
  
function certificacionTipo_combobox ( dom, obj, tipo_certi ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/certificacionestipos/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            



            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['certificacion_tipo'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
            
                                    
            // 
            var domselect = document.getElementById(dom);   
            domselect.onchange  = function() {   
                
                tipo_certi = domselect.value;
                lista_serie(obj, tipo_certi);    
                
            }    
            domselect.onchange();
            //lista_serie(obj, tipo_certi); 

            
            
            
            
        })
        .catch(function(error) {
            console.log(error);            
        });


}


  


   
function form_inicio(){    


        ajax.url = html.url.absolute()+'/consulta/ovpco/htmf/cab.html'; 
        ajax.metodo = "GET";            
        document.getElementById( "cab_form" ).innerHTML =  ajax.public.html();       
        
        form_cab_limpiar();
        
        form_cab_accion();
        
        form_det_html();
       
    
}
        
    
    
    
    
function form_cab_limpiar(){  
    
    
    //cab_clase    
    var form_campos = ["cab_clase", "cab_programa", "cab_actividad", 
        "cab_obj_desde", "cab_obj_hasta",  "cab_ff", "cab_of", "cab_dpto"];

    form_campos.forEach(function (elemento, indice, array) {    
        document.getElementById( elemento ).value = "0"; 
        //console.log(elemento, indice);
    });
    //document.getElementById( "obj_descripcion" ).innerHTML = "";

}




function form_cab_accion(){  
    
    
    //cab_clase    
    var form_campos = ["cab_clase", "cab_programa", "cab_actividad", 
        "cab_obj_desde", "cab_obj_hasta",  "cab_ff", "cab_of", "cab_dpto"];


    form_campos.forEach(function (elemento, indice, array) {            
        document.getElementById( elemento ).addEventListener( 'blur',
            function(event) {   
                //console.log(this.value);
                this.value = fmtNum(this.value); 
            },
            false
        );               
    });
  
    
    
        var btn_cab_buscar = document.getElementById('btn_cab_buscar');         
        btn_cab_buscar.onclick = function(  )
        {
            form_det_data_WS();
        }


}




   
   
function form_det_data_WS(){

    
        var data = 0;


        var apipath = "/api/presupuesto/consulta/ovpco;n1="+document.getElementById( "cab_clase" ).value
                +";n2="+document.getElementById( "cab_programa" ).value
                +";n3="+document.getElementById( "cab_actividad" ).value
                +";obj1="+document.getElementById( "cab_obj_desde" ).value                
                +";obj2="+document.getElementById( "cab_obj_hasta" ).value                
                +";ff="+document.getElementById( "cab_ff" ).value
                +";of="+document.getElementById( "cab_of" ).value
                +";dpto="+document.getElementById( "cab_dpto" ).value
    
    
        loader.inicio();
        xhr =  new XMLHttpRequest();
        //var url = html.url.absolute()+"/api/ordenescompras/"+ordencompra_ordencompra.value;           
        
        var url = html.url.absolute() + apipath;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                //sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:



                        form_datos_UI ( xhr.responseText);

/*
                                             
                        form_main_detalles_json ( ordencompra_ordencompra.value  );
                        ordencomprapagopago_acciones_lista (  ordencompra_ordencompra.value  );
*/

                        break;

                    case 204:                            

                        form_det_limpiar_UI();
                        msg.error.mostrar( "No encontrado" );         
                        break;
                        
                    case 401:                            
                        window.location = html.url.absolute();                                     
                        break;                        

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );    
      
    
}   
   
   
   
  
  
    
function form_det_html(){  
    
    ajax.url = html.url.absolute()+'/consulta/ovpco/htmf/det.html'; 
    ajax.metodo = "GET";            
    document.getElementById( "det_form" ).innerHTML =  ajax.public.html();           
    

}






function form_datos_UI ( json ){

        form.name = "form_ordencompra";
        
        //form.json = json;           
        //form.llenar();         
  
        //var ojson = JSON.parse(json) ; 
        //var i = 0 ; 
        
        tabla.json = json;    
                tabla.id = "consulta-tabla";
                tabla.linea = "ff";
                tabla.tbody_id = "consulta-tb";
                tabla.campos = ['obj', 'vigente', 'prevision', 'compromiso', 'obligado'];                
                tabla.etiquetas = ['obj', 'vigente', 'prevision', 'compromiso', 'obligado'];
  
        
    //    tabla.ini(obj);
        tabla.gene();   
        
        var obj = new Object();
        obj.tablaformat = ['N', 'N', 'N', 'N', 'N'];                       
        tabla.formato(obj);
         
    
         //  suma
         
            var table = document.getElementById( "consulta-tabla" ).getElementsByTagName('tbody')[0];
            
            var sum_vigente = 0;
            var sum_prevision = 0;
            var sum_compromiso = 0;
            var sum_obligado = 0;
                                        
            
            var rows = table.getElementsByTagName('tr');
            for (var i=0 ; i < rows.length; i++)
            {
                sum_vigente =  parseFloat( NumQP(sum_vigente.toString()) ) + 
                        parseFloat( NumQP( table.rows[i].cells[1].innerHTML) ) ;      
                
                sum_prevision =  parseFloat( NumQP(sum_prevision.toString()) ) + 
                        parseFloat( NumQP( table.rows[i].cells[2].innerHTML) ) ;                      
                
                sum_compromiso =  parseFloat( NumQP(sum_compromiso.toString()) ) + 
                        parseFloat( NumQP( table.rows[i].cells[3].innerHTML) ) ;                      
                
                sum_obligado =  parseFloat( NumQP(sum_obligado.toString()) ) + 
                        parseFloat( NumQP( table.rows[i].cells[4].innerHTML) ) ;                      
                

            }
                           
            document.getElementById( "sum_vigente" ).innerHTML =  fmtNum(sum_vigente.toString());           
            document.getElementById( "sum_prevision" ).innerHTML =  fmtNum(sum_prevision.toString());           
            document.getElementById( "sum_compromiso" ).innerHTML =  fmtNum(sum_compromiso.toString());           
            document.getElementById( "sum_obligado" ).innerHTML =  fmtNum(sum_obligado.toString());           
         
         
         
    
        
        
};



function form_det_limpiar_UI(){  
    
    document.getElementById('consulta-tb').innerHTML = "";         
    
    
            document.getElementById( "sum_vigente" ).innerHTML =  fmtNum("0");           
            document.getElementById( "sum_prevision" ).innerHTML =  fmtNum("0");           
            document.getElementById( "sum_compromiso" ).innerHTML =  fmtNum("0");           
            document.getElementById( "sum_obligado" ).innerHTML =  fmtNum("0");           
    
    
};






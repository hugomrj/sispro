/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM.db;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.xml.Global;


/**
 *
 * @author hugom_000
 */
public class ResultadoSet {

    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        
        
    
    public ResultadoSet ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
        
    
    
    
    public ResultSet  resultset ( String sql, Integer page ) throws Exception {

            statement = conexion.getConexion().createStatement();                         
            sql = sql + BasicSQL.limite_offset(page, lineas);
            resultset = statement.executeQuery(sql);     
            //conexion.desconectar();                
            return resultset;                 
            
    }
    
    
    
    
    public ResultSet  resultset ( String sql  ) throws Exception {

            statement = conexion.getConexion().createStatement();                                     
            resultset = statement.executeQuery(sql);     
            //conexion.desconectar();                
            return resultset;                 
            
    }
    
    
    
    

    public Integer  total_registros ( String sql  ) throws Exception {

            Integer registros = 0;        
            statement = conexion.getConexion().createStatement();             
            registros =  BasicSQL.cont_registros(conexion, sql);  
            resultset = statement.executeQuery(sql);     
            //conexion.desconectar();                
            return registros;                 
            
    }
    
    
    public void  close (  ) throws Exception {            
        conexion.desconectar();               
            
    }
        
      
    
    
    
}

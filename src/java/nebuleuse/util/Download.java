package nebuleuse.util;

import jakarta.activation.MimetypesFileTypeMap;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;



public class Download {



public void downloadFile(HttpServletResponse response, 
        String filePath, String file_name )
  throws ServletException, IOException {


    File fileToDownload = new File(filePath);
    FileInputStream in = new FileInputStream(fileToDownload);

    ServletOutputStream out = response.getOutputStream();   
    String mimeType =  new MimetypesFileTypeMap().getContentType(filePath); 

    response.setContentType(mimeType); 
    response.setContentLength(in.available());
    
    
    response.setHeader( "Content-Disposition", "attachment; filename=\""
       + file_name + "\"" );
       //+ fileToDownload.getName() + "\"" );

    response.addHeader("file_name", file_name );
    

    byte[] buffer = new byte[4096];
    int length;
    while ((length = in.read(buffer)) > 0){
       out.write(buffer, 0, length);
    }

    
    
    
    out.flush();
    out.close();
    in.close();
}    




}

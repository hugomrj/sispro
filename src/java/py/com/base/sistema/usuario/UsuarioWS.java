/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.sistema.usuario;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.Date;
import java.util.List;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;


import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.seguridad.Autentificacion;

/**
 * REST Web Service
 * @author hugo
 */


@Path("usuarios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class UsuarioWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    
    Usuario usuario = new Usuario();       
                         
    public UsuarioWS() {
        
    }

    
    
    
    @GET    
    @Path("/lista/") 
    public Response lista ( ) {

        try {                    
           
            if (true)
            {                
                
                UsuarioDAO dao = new UsuarioDAO();
                
                List<Usuario> lista = dao.list(1);                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
        
    
    
    
    @GET    
    public Response list ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                UsuarioDAO dao = new UsuarioDAO();
                
                List<Usuario> lista = dao.list(page);                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    
    
    
    
    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }

            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                UsuarioDAO dao = new UsuarioDAO();
                
                List<Usuario> lista = dao.search(page, q);
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
    
    
    
    @GET
    @Path("/{usu}")
    public Response getUsuario(     
            @HeaderParam("token") String strToken,
            @PathParam ("usu") Integer usu ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();
                
                this.usuario = (Usuario) persistencia.filtrarId(usuario, usu);                  
                String json = gson.toJson(this.usuario);
        
                if (this.usuario == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }else
                {
                    this.usuario.setClave("secret");
                }
                
                return Response
                        .status( this.status )
                        .entity( json )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }        

    }    
      
      
    
    
    
 
    @POST
    public Response addUsuario( 
            @HeaderParam("token") String strToken,
            String json ) {
           
         
        try {                    
            
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                Usuario usuarioReq = new Usuario();
                usuarioReq = gson.fromJson(json, usuarioReq.getClass());
                
                
                this.usuario = new UsuarioDAO().insert(
                            usuarioReq.getCuenta(), 
                            usuarioReq.getClave());
                             
                if (this.usuario == null){
                    System.out.println("usuario vacio");
                    this.status = Response.Status.NO_CONTENT;
                }
                
                
                json = gson.toJson(this.usuario);
                
                return Response
                        .status(this.status)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
      
    
    @PUT    
    @Path("/{usu}")    
    public Response editUsuario (
            String json,
            @HeaderParam("token") String strToken,
            @PathParam ("usu") Integer usu) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                
                Usuario usuarioReq = new Usuario();
                usuarioReq = gson.fromJson(json, usuarioReq.getClass());
                
                this.usuario = new UsuarioDAO().update(
                                            usuarioReq, 
                                            usu);
                
                json = gson.toJson(this.usuario);
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                    
    
        }        
    }    
    
    
    
    
    
    
    
    @DELETE  
    @Path("/{id}")    
    public Response deleteUsuario (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                //filas = new UsuarioDAO().delete(usu);                
                filas = persistencia.delete(usuario, id) ;
                
                
                if (filas != 0){
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{   
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                        
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }               
        
    }    
        

    
    
    
    
    
    @POST
    @Path("/login")
    public  Response login ( String json, @Context HttpServletRequest request ) throws IOException 
      {

            String remotehost = request.getRemoteHost();            
            
            String real_ip = request.getHeader("X-Forwarded-For");            
            
            Date fecha = new Date();
            String sistema = new Global().getValue("sistema");
            
                    
        try {
            
                Usuario usu = new UsuarioDAO().setJson(json);
                                               
                usu  = new UsuarioDAO().login(
                            usu.getCuenta(), 
                            usu.getClave()
                            );
                         
                
            if (usu == null)
            {           
                                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("Authorization Required")
                    .header("token", null)
                    .build();                                        
                
            }
            else
            {
                
                
                if (real_ip == null ){                          
                    real_ip = " null " ;
                }                

                
        System.out.println( sistema + "  " + remotehost + " " + fecha + " " + usu.getCuenta() );            
               
                
                usu.setClave("secret");   
                           
                autorizacion.newTokken(
                        usu.getUsuario().toString(), 
                        usu.getCuenta().trim()
                ) ;

                
                new UsuarioDAO().set_tokenDB(
                        usu.getUsuario().toString(), 
                        autorizacion.token.getIat()
                );
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(null)
                        .header("token", autorizacion.encriptar())
                        .build();                        
            }                      
        } 
        
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       


    
    
    
    
    
    
    
    
    @GET    
    @Path("/controlurl") 
    public Response controlurl ( 
            @HeaderParam("token") String strToken,
            @HeaderParam("path") String strPath
            ) {
        
        try {  
                       
            if (autorizacion.verificar(strToken))
            {                  
               
                UsuarioDAO dao = new UsuarioDAO();
                Integer usu = Integer.parseInt( autorizacion.token.getUser() );
               
              
                if ( dao.IsControlPath(usu, strPath ) )
                {
                    return Response
                            .status(Response.Status.OK)           
                            .header("token", strToken)                            
                            .header("sessionusuario", autorizacion.token.getNombre())
                            .build();                     
                }
                else
                {                
                    return Response
                            .status(Response.Status.UNAUTHORIZED)
                            .build();                            
                } 
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                             
            }        
        }     
        catch (Exception ex) {
                return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("0")
                    .build();                                      
        }      
    }    
        
    
    
    
      
     
    @PUT    
    @Path("/cambiopass")    
    public Response cambiopass (            
            @HeaderParam("token") String strToken,
            String json
            ) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    

                JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);            

                String old = convertedObject.get("old").getAsString() ;
                String jnew = convertedObject.get("new").getAsString() ;
                String usu = autorizacion.token.getUser();

                Integer i = new UsuarioDAO().cambiarPass(usu, old, jnew);
            
                
                if (i == 0)
                {
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                                            
                }
                else
                {   
                    return Response
                            .status(Response.Status.OK)
                            .entity(convertedObject.toString())
                            .header("token", autorizacion.encriptar())
                            .build();                           

                }            

            }
            else{    
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {


            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)                    
                    .entity(ex.getMessage())
                    .header("token", strToken)
                    .build();                    

        }        
    }    
    
    
    
    
        
    

    
    
    
}

package py.com.base.sistema.rol;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class RolSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new Rol(), busqueda);        
        
        return sql ;             
    }        
    

    
    
    public String getuxiliar ( Integer codeuser )
            throws Exception {
    
        String sql = "";
        
        ReaderT reader = new ReaderT("Rol");
        reader.fileExt = "getoficina.sql";
        
        sql = reader.get( codeuser );           
        
        
        return sql ;             
    }        
    
    

    
    
}





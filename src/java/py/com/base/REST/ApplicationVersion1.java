/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    
    

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.sistema.usuario.UsuarioWS.class);
    }
    
}

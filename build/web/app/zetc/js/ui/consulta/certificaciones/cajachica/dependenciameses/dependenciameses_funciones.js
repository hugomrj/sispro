
   
function form_inicio(){    


    var url  = html.url.absolute()+'/consulta/certificaciones'
        +'/cajachica/dependenciameses/htmf/cab.html'; 


    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {
            
            
        document.getElementById( "arti_form" ).innerHTML =  data;       

        form_det_html();

        form_ini();
        
        
      })
 
    
}
        
    
    
    
    
    
    
function form_ini(){  
    

    // buscar dependencia
    var dependencia = document.getElementById('dependencia');   
    dependencia.value = 0;                        
    dependencia.onblur  = function() {                        

        dependencia.value = fmtNum(dependencia.value);      
        dependencia.value = NumQP(dependencia.value);      

        var depen = dependencia.value ;

        ajax.url =  html.url.absolute()+'/api/dependencias/'+depen;

        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {     
                    var ojson = JSON.parse(xhr.responseText) ;     

                    document.getElementById('dependencia').value = ojson['dependencia'];

                    document.getElementById('dependencia_descripcion_out').innerHTML = 
                            ojson['nombre'];
                }
                else{
                    document.getElementById('dependencia').value = 0;
                    document.getElementById('dependencia_descripcion_out').innerHTML = "";
                }       
                
                form_det_data_WS();
                
            })            
     };    



    var ico_more_dependencia = document.getElementById('ico-more-dependencia');
    ico_more_dependencia.addEventListener('click',
        function(event) {     

            var obj = new Dependencia();      

            obj.acctionresul = function(id) {    
                dependencia.value = id; 
                dependencia.onblur(); 
            };       
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
        },
        false
    );        


}
    
    
    
    
    
function form_det_html(){  

    
    var url  = html.url.absolute()+'/consulta/certificaciones'
        +'/cajachica/dependenciameses/htmf/det.html'; 


    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => {            
        document.getElementById( "det_form" ).innerHTML =  data;    


        form_botones_acciones( );


      })

    


}

    


   
   
function form_det_data_WS(){
      

    var depe = document.getElementById('dependencia').value;
                            
    loader.inicio();

    var url = html.url.absolute()+'/api/' + "cajachica/consulta/"
        +depe;   


    ajax.async.json( "get", url, null )
        .then(( xhr ) => {

            loader.fin();

            if (xhr.status == 200)
            {     

                var ojson = JSON.parse( xhr.responseText ) ; 
                tabla.json = JSON.stringify(ojson['datos']) ;  
                
        //tabla.json = json;    
                tabla.id = "consulta-tabla";
                tabla.linea = "mes";
                tabla.tbody_id = "consulta-tb";
                tabla.campos = ['mes', 'monto_certificado'];                
                tabla.etiquetas = ['mes', 'monto_certificado'];                
  
                tabla.gene();  

            
                var obj = new Object();
                obj.tablaformat = ['N', 'N'];                       
                tabla.formato(obj);            

                // sumario                
                var sum_inicial = ojson['summary'][0]['monto_certificado']  ;    
                
                if (!(sum_inicial === undefined)) {    
                    document.getElementById( "sum_certificado" ).innerHTML =  fmtNum(sum_inicial.toString());                
                }   
                else{
                    document.getElementById( "sum_certificado" ).innerHTML =  fmtNum("0");                
                }
                

            }
            else {
            }                  

        })            

    
    
    
}   
   
   
   
  
  





function form_botones_acciones( ){


    var btn_xlsx = document.getElementById( 'btn_xlsx');
    btn_xlsx.onclick = function()
    {  
        
        loader.inicio();
        

        //var url = html.url.absolute() + "/Vacunacion/Download";

        var dependencia = document.getElementById('dependencia');   
        
        var url = html.url.absolute() 
                +'/api/cajachica/xlsx/dependenciameses/'+dependencia.value;

        // Use XMLHttpRequest instead of Jquery $ajax
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            if (xhttp.readyState === 4 && xhttp.status === 200) {

                var file_name = "consulta.xlsx" ; 

                // Trick for making downloadable link
                var a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhttp.response);
                a.download = file_name;
                a.click();
                loader.fin();

            }
        };
        // Post data to URL which handles post request
        xhttp.open("GET", url);
        xhttp.setRequestHeader("Content-Type", "application/json");

        // You should set responseType as blob for binary responses
        xhttp.responseType = 'blob';
        xhttp.setRequestHeader("token", localStorage.getItem('token'));           
        xhttp.send();

    }

        
        
        
};










function Ejedoc_concepto(){
    
   this.tipo = "ejedoc_concepto";   
   this.recurso = "ejecucionesdocumentos";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/ejecucion";   
   
   
   this.campoid=  'id';
   
   /*
   this.tablacampos =  ['clase', 'programa', 'actividad', 'obj',  'ff', 'of',
                'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num', 
                'fecha', 'rcp', 'concepto', 'obligado', 'pagado' ];
   */
   
   
   this.tablacampos =  [ 'clase', 'programa', 'actividad', 'obj', 'ff', 'of',
                        'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];
   
   
   
   this.etiquetas =  [  'clase', 'programa', 'actividad', 'obj', 'ff', 'of',
                        'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];            
            
   this.tablaformat =  [  'C', 'C', 'C', 'C', 'C', 'C', 
                        'U', 'N', 'U', 'Z',
                        'D', 'C', 'C', 'N', 'N'];
   

      
   this.botones_lista = [ this.lista_new] ;
   //this.botones_form = "consulta-acciones";   
  
   
}





Ejedoc_concepto.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





Ejedoc_concepto.prototype.form_ini = function(obj) {    



    var concepto = document.getElementById('concepto');
        concepto.addEventListener('keydown', function(event) {
            
            
            
            //if(event.keyCode == 13){
            if (event.key === "Enter") {

                  
                
            }
            
            
        },
        false
    );    
    
    
    
    
    

    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     

        var ed = new Ejedoc_concepto;
        ed.consulta2_detalle_promesa(obj); 

    }

        
    
};






Ejedoc_concepto.prototype.form_validar = function() {    
    
    return true;
};







Ejedoc_concepto.prototype.main_form = function(obj) {    



    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('cab_form').innerHTML = data ;
        
            obj.form_ini(obj)                
            
      })





    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('det_form').innerHTML = data ;
        
        //reflex.getTituloplu(obj);                                  
            
      })



};





Ejedoc_concepto.prototype.consulta2_detalle_promesa = function( obj  ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            

///api/ejecuciondocumento/consulta1/;clase=0;prog=0;acti=0;obj=0;ff=0;of=0;fe1=;fe2=

   
            var url = html.url.absolute() + '/api/ejecuciondocumento/consulta2/'
                +';concepto='+document.getElementById("concepto").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;
                    

                    var ojson = JSON.parse( tabla.json ) ;     
                    
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                    
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();              
                            
                        tabla.formato(obj);                            
                        
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary']) ;              
                        var ojsumas = JSON.parse(jsumas) ;
                        
                        document.getElementById("sum_obligado").innerHTML
                            =  fmtNum( ojsumas["obligado"] );
                        
                        document.getElementById("sum_pagado").innerHTML
                            =  fmtNum( ojsumas["pagado"] );
                        
                        
                    }
                    else{

                       
                        fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
                          .then(response => {
                            return response.text();
                          })
                          .then(data => {
                            document.getElementById('det_form').innerHTML = data ;
                            msg.error.mostrar("No existen registros");
                          })                       

                        
                    }

  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};




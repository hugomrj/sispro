

function BaseInicialSaldo(){
    
   this.tipo = "baseinicialsaldo";   
   this.recurso = "";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/contrataciones";   
   
   
   this.campoid=  'id';
   
   
   this.tablacampos =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];
   
   
   
   this.etiquetas =  [ 'doc_tipo', 'doc_num', 'ref_tipo', 'ref_num',
                        'fecha', 'rcp', 'concepto', 'obligado', 'pagado'];            
            
   this.tablaformat =  [ 'U', 'N', 'U', 'Z',
                        'D', 'C', 'C', 'N', 'N'];
   

      
   this.botones_lista = [ this.lista_new] ;
   //this.botones_form = "consulta-acciones";   
  
   
}





BaseInicialSaldo.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





BaseInicialSaldo.prototype.form_ini = function(obj) {    


    var clase = document.getElementById('clase');          
    clase.onblur  = function() {                
        clase.value  = fmtNum(clase.value);
    };     
    clase.onblur();       
            
    var programa = document.getElementById('programa');          
    programa.onblur  = function() {                
        programa.value  = fmtNum(programa.value);
    };     
    programa.onblur();       
            
    var actividad = document.getElementById('actividad');          
    actividad.onblur  = function() {                
        actividad.value  = fmtNum(actividad.value);
    };     
    actividad.onblur();       
            
    var objeto = document.getElementById('objeto');          
    objeto.onblur  = function() {                
        objeto.value  = fmtNum(objeto.value);
    };     
    objeto.onblur();       
            
    var ff = document.getElementById('ff');          
    ff.onblur  = function() {                
        ff.value  = fmtNum(ff.value);
    };     
    ff.onblur();       
            
    var of = document.getElementById('of');          
    of.onblur  = function() {                
        of.value  = fmtNum(of.value);
    };     
    of.onblur();       
            

    var dpto = document.getElementById('dpto');          
    dpto.onblur  = function() {                
        dpto.value  = fmtNum(dpto.value);
    };     
    dpto.onblur();       
            
    
    
    
    
    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     
    
        
            var ed = new EjecucionDocumento;
            ed.consulta1_detalle_promesa(obj);
        
        
    }
          
        
    
};






BaseInicialSaldo.prototype.form_validar = function() {    
    
    return true;
};







BaseInicialSaldo.prototype.main_form = function(obj) {    


    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html' )
        .then(response => {
            return response.text();
        })
        .then(data => {        
            document.getElementById('cab_form').innerHTML = data ;        
        })
        .then(data => {
            
            this.ultimafecha(obj);
            obj.form_ini(obj);                
            
        })
            





    fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lin.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('lin_form').innerHTML = data ;        
        //reflex.getTituloplu(obj);                                 
            
      })



};





BaseInicialSaldo.prototype.consulta1_detalle_promesa = function( obj  ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            
   
            var url = html.url.absolute() + '/api/ejecuciondocumento/consulta1/'
                +';clase='+document.getElementById("clase").value 
                +';prog='+document.getElementById("programa").value 
                +';acti='+document.getElementById("actividad").value 
                +';obj='+document.getElementById("objeto").value 
                +';ff='+document.getElementById("ff").value 
                +';of='+document.getElementById("of").value 
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;

                    var ojson = JSON.parse( tabla.json ) ; 
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                    
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();              
                            
                        tabla.formato(obj);                            
                        
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary']) ;              
                        var ojsumas = JSON.parse(jsumas) ;
                        
                        document.getElementById("sum_obligado").innerHTML
                            =  fmtNum( ojsumas["obligado"] );
                        
                        document.getElementById("sum_pagado").innerHTML
                            =  fmtNum( ojsumas["pagado"] );
                        
                        
                    }
                    else{
                       
                        
                       
                        fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
                          .then(response => {
                            return response.text();
                          })
                          .then(data => {
                            document.getElementById('det_form').innerHTML = data ;
                            msg.error.mostrar("No existen registros");
                          })                       
                       
                        
                    }

  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};







BaseInicialSaldo.prototype.ultimafecha = function( obj  ) {    

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var url = html.url.absolute() + '/api/basefoxfecha/ultimafecha'
        var xhr =  new XMLHttpRequest();      

        var metodo = "GET";                                     
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                var json = xhr.responseText;

console.log(json);

                var ojson = JSON.parse(json) ; 
                
                var migracion_fecha = document.getElementById('migracion_fecha');                
                migracion_fecha.innerHTML = ojson["fecha"];
                
                var migracion_codigo = document.getElementById('migracion_codigo');                
                migracion_codigo.value = ojson["fecha_id"];
                
                loader.fin();
            }
        };

        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           
        xhr.send( null );                       

    })

    return promise;

};



